(function ($) {
  Drupal.behaviors.joyride_help = {
    attach: function (context) {
      // Hide the content
      $('[data-joyride=data-joyride]').hide();

      // Attach the click event
      $('.joyride-help-button').each(function(){
        $(this).click(function(event){
          event.preventDefault();
          $(this).parent('h3').siblings('[data-joyride=data-joyride]').joyride({autoStart: true});
        });
      });

      // Automatically start the joyride when the tour is set to TRUE
      if ($.url().param('joyride-tour') != undefined) {
        $('[data-joyride=data-joyride]').joyride({autoStart: true});
      }
    }
  };
})(jQuery);