<?php

/**
 * @file
 * Joyride Help module
 */

/**
 * Implements hook_library().
 */
function joyride_help_library() {
  // Joyride Help
  $libraries['joyride_help_tour'] = array(
    'title' => 'Joyride Help Tour',
    'website' => 'http://drupal.org/project/joyride_help',
    'version' => '1.0',
    'js' => array(
      drupal_get_path('module', 'joyride_help') . '/joyride_help.js' => array('scope' => 'footer'),
    ),
    'dependencies' => array(
      // Depende on the joyride library
      array('joyride_help', 'joyride'),
      array('joyride_help', 'purl'),
    ),
  );

  $libraries['joyride'] = array(
    'title' => 'Joyride Tour Library',
    'website' => 'http://drupal.org/project/joyride_help',
    'version' => '2.1.0',
    'js' => array(
      libraries_get_path('joyride') . '/modernizr.mq.js' => array('scope' => 'footer'),
      libraries_get_path('joyride') . '/jquery.joyride-2.1.js' => array('scope' => 'footer'),
    ),
    'css' => array(
      libraries_get_path('joyride') . '/joyride-2.1.css' => array(),
    ),
  );

  $libraries['purl'] = array(
    'title' => 'Purl (A JavaScript URL parser)',
    'website' => 'https://github.com/allmarkedup/purl/tree/v2.3.1',
    'version' => '2.3.1',
    'js' => array(
      libraries_get_path('purl') . '/purl.js' => array('scope' => 'footer'),
    ),
  );


  return $libraries;
}

/**
 * Implements hook_theme().
 */
function joyride_help_theme($existing, $type, $theme, $path) {
  return array(
    'joyride_help_tour' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_link' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_timer' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_tip' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_wrapper' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_button' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_modal' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_expose' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
    'joyride_help_expose_cover' => array(
      'render element' => 'element',
      'file' => 'joyride_help.theme.inc',
    ),
  );
}

/**
 * Implements hook_styleguide().
 */
function joyride_help_styleguide() {
  // @todo
}

/**
 * Generate a joyride tour
 *
 * @param array $variables
 *  Array of options to generate your tour
 *
 *  Ex: array(
 *        'items' => array(
 *          0 => array(
 *            'title' => t('Step Title'),
 *            'body' => t('My content for the first step'),
 *            'text_format' => 'plain',
 *            'options' => array(see data options...),
 *            'data-id' => 'my-element-id',
 *            'data-text' => t('Next'),
 *            'data-button' => t('Button text'),
 *          ),
 *          1 => array(
 *            'body' => '<p>' . t('Body only step') . '</p>', // Body is rendered without a text_format, default to filter_xss().
 *          ),
 *        ),
 *        'attributes' => array(),
 *        'type' => 'ol',
 *        'title' => '',
 *      );
 *
 * @return string
 *  Returns the rendered tour markup.
 * @see theme_item_list().
 */
function joyride_help_tour($variables) {
  return theme('joyride_help_tour', $variables);
}

/**
 * Implements hook_help().
 */
function joyride_help_help($path, $arg) {
  switch ($path) {
    // Main module help for the block module
    case 'admin/help#joyride_help':
      return joyride_help_tour(_joyride_help_tour_content());

    // Help for another path in the block module
    case 'admin/config/system/joyride-help':
      return joyride_help_tour(_joyride_help_tour_content());
  }
}

/**
 * Default tour content for joyride help
 */
function _joyride_help_tour_content() {

  $options = _joyride_help_options();

  $out = array();
  foreach($options as $key => $value) {
    $out[] = "$key : $value";
  }

  return array(
    'items' => array(
      array(
        'title' => t('Using Joyride Help'),
        'body' => t('Joyride help allows you to build interactive help in Drupal.'),
      ),
      array(
        'title' => t('Use the existing "help" sections'),
        'body' => t('Help is printed out through hook_help() meaning it appears in the existing help block in Drupal (by default in the "help" region of your theme).')
      ),
      array(
        'title' => t('Give context to your help docs'),
        'body' => t('Guide your users directly to the point on the page where your help makes sense.'),
      ),
      array(
        'title' => t('Lots of options'),
        'body' => t('You can pass in lots of options to customize your Joyride tours. You can find more developer information in the README.md file.' . theme('item_list', array('items' => $out))),
        'data-button' => t('Done'),
      ),
    ),
    'type' => 'ol',
    'title' => t('Help me out'),
    'attributes' => array('id' => 'joyride-help-content'),
  );
}

/**
 * List of supported options with Joyride
 *
 * @return array
 *  Returns the array of options.
 */
function _joyride_help_options() {
  return array(
    'expose'                   => false,     // turn on or off the expose feature
    'modal'                    => true,      // Whether to cover page with modal during the tour
    'tip_location'             => 'bottom',  // 'top' or 'bottom' in relation to parent
    'nub_position'             => 'auto',    // override on a per tooltip bases
    'scroll_speed'             => 1500,      // Page scrolling speed in milliseconds, 0 = no scroll animation
    'scroll_animation'         => 'linear',  // supports 'swing' and 'linear', extend with jQuery UI.
    'timer'                    => 0,         // 0 = no timer , all other numbers = timer in milliseconds
    'start_timer_on_click'     => true,      // true or false - true requires clicking the first button start the timer
    'start_offset'             => 0,         // the index of the tooltip you want to start on (index of the li)
    'next_button'              => true,      // true or false to control whether a next button is used
    'tip_animation'            => 'fade',    // 'pop' or 'fade' in each tip
    'tip_animation_fade_speed' => 300,       // when tipAnimation = 'fade' this is speed in milliseconds for the transition
    'cookie_monster'           => false,     // true or false to control whether cookies are used
    'cookie_name'              => 'joyride', // Name the cookie you'll use
    'cookie_domain'            => false,     // Will this cookie be attached to a domain, ie. '.notableapp.com'
    'cookie_expires'           => 365,       // set when you would like the cookie to expire.
    'tip_container'            => 'body',    // Where will the tip be attached

    /*
      pause_after              : [],        // array of indexes where to pause the tour after
      exposed                  : [],        // array of expose elements

      tip_location_patterns    : {
      top: ['bottom'],
      bottom: [], // bottom should not need to be repositioned
      left: ['right', 'top', 'bottom'],
      right: ['left', 'top', 'bottom']
    },
    post_ride_callback     : function (){},    // A method to call once the tour closes (canceled or complete)
    post_step_callback     : function (){},    // A method to call after each step
    pre_step_callback      : function (){},    // A method to call before each step
    pre_ride_callback      : function (){},    // A method to call before the tour starts (passed index, tip, and cloned exposed element)
    post_expose_callback   : function (){},    // A method to call after an element has been exposed
    template : { // HTML segments for tip layout
      link    : '<a href="#close" class="joyride-close-tip">&times;</a>',
      timer   : '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',
      tip     : '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',
      wrapper : '<div class="joyride-content-wrapper"></div>',
      button  : '<a href="#" class="small button joyride-next-tip"></a>',
      modal   : '<div class="joyride-modal-bg"></div>',
      expose  : '<div class="joyride-expose-wrapper"></div>',
      expose_cover: '<div class="joyride-expose-cover"></div>'
    },
    expose_add_class : '' // One or more space-separated class names to be added to exposed element
      */
  );
}

/**
 * Prepare the tour item for rendering.
 */
function _joyride_help_tour_item($item) {
  $tour_item = array();

  $tour_item['data'] = '';

  if (!empty($item['title'])) {
    $tour_item['data'] .= '<h4>' . check_plain($item['title']) . '</h4>';
  }

  // Text must be run through a text formatter.
  if (!empty($item['body'])) {
    if (!empty($item['text_format'])) {
      $tour_item['data'] .= check_markup($item['body'], $item['text_format']);
    }
    else {
      $tour_item['data'] .= '<p>' . filter_xss($item['body']) . '</p>';
    }
  }

  // Add embedded youtube videos
  if (!empty($item['embed-youtube'])) {
    $tour_item['data'] .= '<div><iframe width="256" height="144" src="' . filter_xss($item['embed-youtube']) . '" frameborder="0" allowfullscreen></iframe></div>';
  }

  // Element ID selector to attach tour step to
  if (!empty($item['data-id'])) {
    $tour_item['data-id'] = check_plain($item['data-id']);
  }

  if (!empty($item['data-class'])) {
    $tour_item['data-class'] = check_plain($item['data-class']);
  }

  if (!empty($item['data-text'])) {
    $tour_item['data-text'] = check_plain($item['data-text']);
  }

  if (!empty($item['data-button'])) {
    $tour_item['data-button'] = check_plain($item['data-button']);
  }

  if (!empty($item['options'])) {
    $out = array();
    foreach($item['options'] as $key => $value) {
      $out[] = "$key:$value";
    }

    $tour_item['data-options'] = implode(';', $out);
  }

  return $tour_item;
}
