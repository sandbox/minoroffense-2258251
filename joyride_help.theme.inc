<?php

/**
 * @file
 * Theme functions
 */

/**
 * Default process callback for joyride_help_tour().
 */
function template_process_joyride_help_tour(&$variables) {
  // Prepare variables
  $list = array(
    'items' => array(),
    'attributes' => array('data-joyride' => 'data-joyride', 'class' => array('joyride-list')),
    'title' => (!empty($variables['title']) and is_string($variables['title'])) ? $variables['title'] : '',
    'type' => ('ol' == $variables['type'] or 'ul' == $variables['type']) ? $variables['type'] : 'ol',
  );

  // Merge attributes
  if (!empty($variables['attributes']) and is_array($variables['attributes'])) {
    $list['attributes'] += $variables['attributes'];
  }

  if (!empty($list['title'])) {
    $list['title'] = l($list['title'], request_path(), array('attributes' => array('class' => array('joyride-help-button'), 'id' => 'joyride-help-link'), 'fragment' => 'joyride'));
  }

  foreach($variables['items'] as $item) {
    $list['items'][] = _joyride_help_tour_item($item);
  }

  // Build the tour list
  $list = theme('item_list', $list);

  // Build the render array
  $variables['tour'] = array(
    '#markup' => $list,
    '#attached' => array(
      'library' => array(
        array('joyride_help', 'joyride_help_tour'),
      ),
    ),
  );
}

/**
 * Default implementation of joyride_help_tour().
 */
function theme_joyride_help_tour($variables) {
  return drupal_render($variables['tour']);
}

/**
 * Default implementation of joyride_help_link().
 */
function theme_joyride_help_link($variables) {
  return '<a href="#close" class="joyride-close-tip">&times;</a>';
}

/**
 * Default implementation of joyride_help_timer().
 */
function theme_joyride_help_timer($variables) {
  return '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>';
}

/**
 * Default implementation of joyride_help_tip().
 */
function theme_joyride_help_tip($variables) {
  return '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>';
}

/**
 * Default implementation of joyride_help_wrapper().
 */
function theme_joyride_help_wrapper($variables) {
  return '<div class="joyride-content-wrapper"></div>';
}

/**
 * Default implementation of joyride_help_button().
 */
function theme_joyride_help_button($variables) {
  return '<a href="#" class="small button joyride-next-tip"></a>';
}

/**
 * Default implementation of joyride_help_modal().
 */
function theme_joyride_help_modal($variables) {
  return '<div class="joyride-modal-bg"></div>';
}

/**
 * Default implementation of joyride_help_expose().
 */
function theme_joyride_help_expose($variables) {
  return '<div class="joyride-expose-wrapper"></div>';
}

/**
 * Default implementation of joyride_help_expose_cover().
 */
function theme_joyride_help_expose_cover($variables) {
  return '<div class="joyride-expose-cover"></div>';
}
