Joyride Help
============

The joyride help modules uses Drupal help provided by hook_help() implementations and creates Joyride guided help tours of your site.  The data returned from the hook_help() function is used to generate the tour and follows this format:

````php
array(
  'items' => array(
    0 => array(
      'title' => t('Step Title'),
      'body' => t('My content for the first step'),
      'text_format' => 'plain',
      'options' => array(see Data Options...),
      'data-id' => 'my-element-id',
      'data-text' => t('Next'),
      'data-button' => t('Button text'),
    ),
    1 => array(
      'body' => '<p>' . t('Body only step') . '</p>', // Body is rendered without a text_format, default to filter_xss().
    ),
  ),
  'attributes' => array(),
  'type' => 'ol',
  'title' => '',
);
````


### Data Options

````
{
  'tipLocation' : 'bottom', // 'top' or 'bottom' in relation to parent
  'nubPosition' : 'auto', // override on a per tooltip bases
  'scroll' : true, // whether to scroll to tips
  'scrollSpeed' : 300, // Page scrolling speed in milliseconds
  'timer' : 0, // 0 = no timer , all other numbers = timer in milliseconds
  'autoStart' : false, // true or false - false tour starts when restart called
  'startTimerOnClick' : true, // true or false - true requires clicking the first button start the timer
  'startOffset' : 0, // the index of the tooltip you want to start on (index of the li)
  'nextButton' : true, // true or false to control whether a next button is used
  'tipAnimation' : 'fade', // 'pop' or 'fade' in each tip
  'pauseAfter' : [], // array of indexes where to pause the tour after
  'tipAnimationFadeSpeed': 300, // when tipAnimation = 'fade' this is speed in milliseconds for the transition
  'cookieMonster' : false, // true or false to control whether cookies are used
  'cookieName' : 'joyride', // Name the cookie you'll use
  'cookieDomain' : false, // Will this cookie be attached to a domain, ie. '.notableapp.com'
  'cookiePath' : false, // Set to '/' if you want the cookie for the whole website
  'localStorage' : false, // true or false to control whether localstorage is used
  'localStorageKey' : 'joyride', // Keyname in localstorage
  'tipContainer' : 'body', // Where will the tip be attached
  'modal' : false, // Whether to cover page with modal during the tour
  'expose' : false, // Whether to expose the elements at each step in the tour (requires modal:true)
  'postExposeCallback' : $.noop, // A method to call after an element has been exposed
  'preRideCallback' : $.noop, // A method to call before the tour starts (passed index, tip, and cloned exposed element)
  'postRideCallback' : $.noop, // A method to call once the tour closes (canceled or complete)
  'preStepCallback' : $.noop, // A method to call before each step
  'postStepCallback' : $.noop, // A method to call after each step
  'template' : { // HTML segments for tip layout
    'link' : '<a href="#close" class="joyride-close-tip">X</a>',
    'timer' : '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',
    'tip' : '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',
    'wrapper' : '<div class="joyride-content-wrapper" role="dialog"></div>',
    'button' : '<a href="#" class="joyride-next-tip"></a>',
    'modal' : '<div class="joyride-modal-bg"></div>',
    'expose' : '<div class="joyride-expose-wrapper"></div>',
    'exposeCover': '<div class="joyride-expose-cover"></div>'
  }
},
````

Note that configurations that are objects, functions, or arrays can only be passed in during intitialization.
